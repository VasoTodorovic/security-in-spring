# Security in Spring


![a]( images/1.png "a")
We mainly use the default configurations to prove that the correct dependencies are in place.

* The authentication filter delegates the authentication request to the authentication manager and, based on the response, configures the security context.

* The authentication manager uses the authentication provider to process authentication.

* The authentication provider implements the authentication logic.

* The user details service implements user management responsibility, which the authentication provider uses in the authentication logic.

* The password encoder implements password management, which the authentication provider uses in the authentication logic.

* The security context keeps the authentication data after the authentication process.

UserDetailsService contract with Spring Security manages the details about users.

These default credentials are “user” This password is randomly generated when the Spring context is loaded
The PasswordEncoder does two things:

* Encodes a password

* Verifies if the password matches an existing encoding

Basic authentication only requires the client to send a username and a password through the HTTP Authorization header

Base64 is only an encoding method for the convenience of the transfer; it’s not an encryption or hashing method. While in transit, if intercepted, anyone can see the credentials


[WebConfigureAdapter from book is no longer in use ](https://spring.io/blog/2022/02/21/spring-security-without-the-websecurityconfigureradapter)

**UserDetailsService** is only responsible for retrieving the user by username

**The UserDetailsManager** adds behavior that refers to adding, modifying, or deleting the user, which is a required functionality in most applications.

**Pasword Encoder** is used by Provider to check if password is correct

Spring Security represents the actions that a user can do with the **GrantedAuthority** interface. We often call these authorities, and a user has one or more authorities

**UserDetails** contract represents the user as understood by Spring Security. The class of your application that describes the user has to implement this interface, and in this way, the framework understands it.

The authorities represent what the user can do in your application. Without authorities, all users would be equal.

when we needed to configure HTTP POST, we also had to add a supplementary instruction to the configuration to disable CSRF protection. The reason why you can’t directly call an endpoint with HTTP POST is because of CSRF protection, which is enabled by default in Spring Security
What you know for sure is that before being able to do any action that could change data, a user must send a request using HTTP GET to see the web page at least once. When this happens, the application generates a unique token. The application now accepts only requests for mutating operations (POST, PUT, DELETE, and so forth) that contain this unique value in the header.