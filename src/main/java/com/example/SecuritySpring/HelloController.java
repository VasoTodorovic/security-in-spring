package com.example.SecuritySpring;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @GetMapping("/public")
    public String getPublic() {
        return "Hello!";
    }
    @GetMapping("/authorization")
    public String getAuthorized() {
        return "Hello!";
    }
    @PostMapping("/authorization")
    public String postAuthorized() {
        return "Hello!";
    }
}
