package com.example.SecuritySpring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;


import java.util.ArrayList;
import java.util.List;


@Configuration
public class ProjectConfig extends WebSecurityConfigurerAdapter {

    @Override
    @Bean
    public UserDetailsService userDetailsService() {
        InMemoryUserDetailsManager userDetailsService = new InMemoryUserDetailsManager();

        UserDetails user1 = User.withUsername("john")
                .password("12345")
                .authorities("read")
                .roles("cutomer")
                .build();
        UserDetails user2 = User.withUsername("jamy")
                .password("2345")
                .authorities("read")
                .roles("ADMIN")
                .build();
        List<UserDetails> list=new ArrayList<>();

                list.add(user1);
        list.add(user2);

        for(UserDetails i:list){
            userDetailsService.createUser(i);
        }

        return userDetailsService;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic();
        http.csrf().disable()
                .authorizeRequests()
                .mvcMatchers(HttpMethod.POST,"/authorization").hasRole("ADMIN")
                .mvcMatchers("/public").permitAll()
                .anyRequest().authenticated();
    }




}
