package com.example.SecuritySpring;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class SecuritySpringApplicationTests {

    @Autowired
    private MockMvc mvc;

    @Test
    @DisplayName("Endpoint public can be called unauthenticated")
    public void testEndpointWithoutAuthentication() throws Exception {
        mvc.perform(get("/public"))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Endpoint cannot be called unauthenticated")
    public void testFailedAuthentication() throws Exception {
        mvc.perform(get("/authorization"))
                .andExpect(unauthenticated());

        mvc.perform(post("/authorization"))
                .andExpect(unauthenticated());

    }
//Both jon and jamy can call GET
    @Test
    @DisplayName("Any authenticated user can call GET /authorization")
    @WithUserDetails("john")
    public void testAnyAuthenticatedUserCanCallGETA() throws Exception {
        mvc.perform(get("/authorization"))
                .andExpect(status().isOk());
    }
//Just jamy can call post becouse of ADMIN role
    @Test
    @DisplayName("Any authenticated user can call GET /authorization")
    @WithUserDetails("jamy")
    public void testAnyUserCanCallPOSTA() throws Exception {
        mvc.perform(post("/authorization"))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Users that dont have role  ADMIN can't call POST /authorization ")
    @WithUserDetails("john")
    public void testEndpointsIsUnauthorized() throws Exception {
        mvc.perform(post("/authorization"))
                .andExpect(status().isForbidden());

    }

    @Test
    @DisplayName("Users with role ADMIN can  call POST /authorization ")
    @WithUserDetails("jamy")
    public void testEndpointsIsAuthorized() throws Exception {
        mvc.perform(post("/authorization"))
                .andExpect(status().isOk());

    }
}